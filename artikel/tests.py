from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .models import Artikel, Comment
from .apps import ArtikelConfig
from .views import artikel_homepage, artikel1
from .forms import CommentForm

# Create your tests here.
class TestArtikellApp(TestCase):
	def test_apps(self):
		self.assertEqual(ArtikelConfig.name, 'artikel')
		self.assertEqual(apps.get_app_config('artikel').name, 'artikel')

class artikelTest(TestCase):
	def test_exist(self):
		response = Client().get('/artikel/')
		self.assertEqual(response.status_code, 200)

	def test_list_mobil_index_func(self):
		found = resolve('/artikel/artikel/')
		self.assertEqual(found.func, artikel_homepage)

	def test_model_can_print(self):
		artikel = Artikel.objects.create(judul="a", penulis="b", isi="1")
		artikel.save()
		self.assertEqual(artikel.__str__(), "a")

		komen = Comment.objects.create(aid=artikel, comment="b")
		komen.save()
		self.assertEqual(komen.__str__(), "anonymous")

# class TestArtikel1(TestCase):
# 	def test_detail_mobil_url_is_exist(self):
# 		artikel = Artikel.objects.create(judul="a", penulis="b", isi="1")
# 		artikel.save()
# 		komen = Comment.objects.create(aid=artikel, comment="b")
# 		komen.save()
# 		form = CommentForm()

# 		response = Client().post('artikel/artikel1.html', data={'artikel' : artikel, 'form':form})
# 		self.assertEqual(response.status_code, 200)