from django.db import models
from django.utils import timezone


# Create your models here.
class Comment(models.Model):
	aid = models.ForeignKey('Artikel', on_delete=models.CASCADE, null=True)
	comment = models.TextField(null=True)
	commenter = models.CharField(default='anonymous', unique=False, max_length=3000)
	waktu_comment = models.DateTimeField(default=timezone.now)

	def __str__(self):
		return self.commenter

class Artikel(models.Model):
	judul = models.CharField('Judul', max_length=3000)
	penulis = models.CharField('Penulis', max_length=3000)
	waktu_tulis = models.DateTimeField(default=timezone.now)
	isi = models.TextField()
	image = models.ImageField(default="static/img/McLaren-sports-car-picture-2018.jpg")
	
	def __str__(self):
		return self.judul