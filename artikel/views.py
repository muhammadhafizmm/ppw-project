from django.shortcuts import render, redirect
from django.http import HttpResponseRedirect
from .models import Comment, Artikel
from .forms import CommentForm

# Create your views here.
def artikel_homepage(request):
	list_artikel = Artikel.objects.all()
	return render(request, 'artikel/artikel_home.html', {'list_artikel' : list_artikel})

def artikel1(request, id):
	form = CommentForm()
	artikel = Artikel.objects.get(id = id)
	# list_comment = Comment.objects.filter(aid=id)
	if request.method == 'POST':
		form = CommentForm(request.POST)
		if form.is_valid():
			form.save()
			return render(request, 'artikel/artikel1.html')
	return render(request, 'artikel/artikel1.html', {'artikel' : artikel, 'form':form})

