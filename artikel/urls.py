from django.urls import path
from . import views

app_name = 'artikel'

urlpatterns = [
    path('', views.artikel_homepage),
    path('artikel/', views.artikel_homepage, name="artikelPage"),
    path('artikel/details/<int:id>/', views.artikel1),
]