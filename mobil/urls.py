from django.urls import path
from . import views

app_name = 'mobil'

urlpatterns = [
    path('', views.list_mobil, name='list_mobil'),
    path('getAllCarData/', views.getAllCarData, name='getAllCarData'),
    path('tambah/', views.tambah_mobil, name='tambah_mobil'),
    path('detail/<int:index>/', views.detail_mobil, name='detail_mobil'),
    path('add-fav/', views.add_fav, name='add_fav'),
]