from django.contrib.auth.models import User
from django.http import HttpResponseForbidden, HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.csrf import csrf_exempt
from django.core.serializers import serialize
from ulasan.models import Review
from .models import Car
from .forms import CarForm

# Create your views here.
def list_mobil(request):
	return render(request, 'mobil/car_list.html')

def detail_mobil(request, index):
	car = Car.objects.get(pk=index)
	fav = False
	if request.user.is_authenticated:
		if request.user.profile in car.profile_set.all():
			fav = True

	return render(request, 'mobil/car_detail.html', {'car': car, "fav": fav})

def tambah_mobil(request):
	if request.method == 'POST':
		form = CarForm(request.POST, request.FILES)
		if form.is_valid():
			form.save()
			return redirect('/mobil/')
	else:
		form = CarForm()
	return render(request, 'mobil/add_car.html', {'form': form})

@csrf_exempt
def add_fav(request):
	if request.method == 'POST':
		car = Car.objects.get(pk=request.POST['id'])
		current = request.user
		
		if current.profile in car.profile_set.all():
			current.profile.favorit.remove(car)
		else:
			current.profile.favorit.add(car)

		current.save()

	return HttpResponseForbidden()

def getAllCarData(request):
	car_list = Car.objects.all()
	car_list = serialize("json", car_list)
	return HttpResponse(car_list, content_type="text/json-comment-filtered")