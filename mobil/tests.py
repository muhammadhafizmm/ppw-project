from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from django.urls import resolve
from django.apps import apps
from django.contrib.auth import authenticate, login
from django.contrib.auth.models import User

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time

from mobil.views import list_mobil, detail_mobil, tambah_mobil, add_fav
from mobil.models import Car
from ulasan.views import tulis_ulasan
from ulasan.models import Review
from .apps import MobilConfig

# Create your tests here.
class TestMobilApp(TestCase):
	def test_apps(self):
		self.assertEqual(MobilConfig.name, 'mobil')
		self.assertEqual(apps.get_app_config('mobil').name, 'mobil')

class TestListMobil(TestCase):
	def test_list_mobil_url_is_exist(self):
		response = Client().get('/mobil/')
		self.assertEqual(response.status_code, 200)

	def test_list_mobil_index_func(self):
		found = resolve('/mobil/')
		self.assertEqual(found.func, list_mobil)

	def test_list_mobil_using_template(self):
		response = Client().get('/mobil/')
		self.assertTemplateUsed(response, 'mobil/car_list.html')

class TestTambahMobil(TestCase):
	def test_tambah_mobil_url_is_exist(self):
		response = Client().get('/mobil/tambah/')
		self.assertEqual(response.status_code, 200)

	def test_tambah_mobil_index_func(self):
		found = resolve('/mobil/tambah/')
		self.assertEqual(found.func, tambah_mobil)

	def test_tambah_mobil_using_template(self):
		response = Client().get('/mobil/tambah/')
		self.assertTemplateUsed(response, 'mobil/add_car.html')

	def test_form_car_works(self):
		context = {
			'mobil':'a',
			'pemilik':'a',
			'tahun':10,
			'kota':'a',
			'harga':'a',
		}
		response = Client().post('/mobil/tambah/', data=context)
		self.assertEqual(response.status_code, 302)

class TestDetailMobil(TestCase):
	def setUp(self):
		mobil = Car(mobil="a", pemilik="b", tahun="1", kota="c", harga="d")
		mobil.save()

	def test_detail_mobil_url_is_exist(self):
		mobil = Car.objects.get(pk=1)
		ulasan1 = Review.objects.create(car=mobil, nama="a", review="b")
		ulasan1.save()
		ulasan2 = Review.objects.create(car=mobil, nama="c", review="d")
		ulasan2.save()

		response = Client().get('/mobil/detail/1/')
		self.assertEqual(response.status_code, 200)

	def test_model_can_print(self):
		mobil = Car.objects.create(mobil='a', pemilik='a', tahun=10, kota='a', harga='a', deskripsi="abc")
		self.assertEqual(mobil.deskripsiSnippet(), "abc...")

class TestAddFavMobil(TestCase):
	def test_add_fav_mobil_url_is_exist(self):
		response = Client().get('/mobil/add-fav/')
		self.assertEqual(response.status_code, 403)

	def test_add_fav_mobil_index_func(self):
		found = resolve('/mobil/add-fav/')
		self.assertEqual(found.func, add_fav)

class FunctionalTest(StaticLiveServerTestCase):
	def setUp(self):
		chrome_options = Options()
		chrome_options.add_argument('--dns-prefetch-disable')
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		chrome_options.add_argument('disable-gpu')

		self.browser = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
		super(FunctionalTest, self).setUp()

	def tearDown(self):
		self.browser.quit()
		super(FunctionalTest, self).tearDown()

	def test_fav_a_car(self):
		self.browser.get(self.live_server_url)
		time.sleep(3)

		# Create an account and login
		User.objects.create_user('abc', 'abc@gmail.com', 'hajakala')
		self.client = Client()
		self.client.login(username='abc', password='hajakala')
		cookie = self.client.cookies['sessionid']
		self.browser.add_cookie({'name': 'sessionid', 'value': cookie.value, 'secure': False, 'path': '/'})
		self.browser.refresh() #need to update page for logged in user
		time.sleep(3)

		# Create Car objects
		car = Car(
				mobil='Avanza', 
				pemilik='Farah',
				tahun=2010,	
				kota='Depok', 
				harga='Rp100.000',
			)
		car.save()

		# List for favorite car(s) is still empty
		akun = self.browser.find_element_by_id('drop-akun')
		akun.click()
		profile = self.browser.find_element_by_id('profile')
		profile.click()
		time.sleep(3)
		self.assertNotIn('Avanza', self.browser.page_source)
		time.sleep(3)

		# Punya Fe
		# Open car's detail page
		rental = self.browser.find_element_by_id('rental')
		rental.click()
		time.sleep(3)

		car_list = self.browser.find_element_by_id('car_list')
		car_list.click()
		time.sleep(3)

		# Selipin punya Apis
		# nyari pake nama
		searchBox = self.browser.find_element_by_id('search-bar')
		searchBox.send_keys("Avanza")
		time.sleep(3)
		suggestion = self.browser.find_element_by_css_selector('span.name-suggest')
		self.assertEqual(suggestion.text, "Avanza")
		time.sleep(3)

		# nyari pake harga
		searchBox.clear()
		searchBox.send_keys("100000")
		time.sleep(3)
		suggestion = self.browser.find_element_by_css_selector('span.name-suggest')
		self.assertEqual(suggestion.text, "Avanza")
		time.sleep(3)

		# tutup lagi fitur ini
		overlaySearchBar = self.browser.find_element_by_css_selector("div.overlay-background")
		overlaySearchBar.click()
		
		# lanjut punya fe
		mobil = self.browser.find_element_by_class_name('card')
		mobil.click()
		time.sleep(3)

		# We haven't favorited this car, so the heart icon is still black
		# Now click the heart to favorite that car
		self.assertIn('black', self.browser.page_source)
		heart = self.browser.find_element_by_class_name('click-heart')
		heart.click()
		time.sleep(3)

		# Since it has been favorited, the heart is red colored
		# You can click it to unfavorite, and click it again to favorite
		self.browser.refresh()
		self.assertIn('red', self.browser.page_source)

		# Now in favorite list there is Avanza
		akun = self.browser.find_element_by_id('drop-akun')
		akun.click()
		time.sleep(3)
		profile = self.browser.find_element_by_id('profile')
		print("imhere")
		profile.click()
		time.sleep(3)
		print("imhere")
		self.assertIn('Avanza', self.browser.page_source)


		# If you press the eart, it will unfavorite the car
		print("imhere")
		heart = self.browser.find_element_by_class_name('click-heart')
		print("imhere")
		heart.click()
		print("imhere")
		self.assertNotIn('Avanza', self.browser.page_source)
		time.sleep(3)
		print("iamhere")