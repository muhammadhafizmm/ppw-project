from django.forms import ModelForm, forms, Textarea
from .models import Car

class CarForm(ModelForm):
	required_css_class = 'required'
	class Meta:
		model = Car
		widgets = {
			'deskripsi': Textarea(attrs={'rows':4, 'cols':19}),
		}
		fields = [
			'mobil',
			'pemilik',
			'tahun',
			'kota',
			'harga',
			'deskripsi',
			'foto'
		]
