from django.contrib import admin
from .models import Car

# Register your models here.
@admin.register(Car)
class CarAdmin(admin.ModelAdmin):
	list_display = ('mobil', 'pemilik', 'tahun', 'kota', 'harga', 'deskripsi')
	ordering = ('mobil', 'kota')
	search_fields = ('mobil', 'kota')