from django.db import models

# Create your models here.
class Car(models.Model):
	mobil = models.CharField('Nama Mobil', max_length=120, null=True)
	pemilik = models.CharField('Nama Pemilik', max_length=120, null=True)
	tahun = models.IntegerField('Tahun Keluaran', null=True)
	kota = models.CharField('Kota', max_length=120, null=True)
	harga = models.CharField('Harga Sewa per Hari', max_length=20, null=True, help_text='contoh : Rp100.000')
	deskripsi = models.TextField('Deskripsi', blank=True)
	foto = models.ImageField('Unggah Foto', upload_to='static/img/mobil/', null=True, blank=True)
	sewa = models.BooleanField(default=True)
	rating = models.IntegerField('Rating', default=5)

	def deskripsiSnippet(self):
		return self.deskripsi[:50] + "..."
