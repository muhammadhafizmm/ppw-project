from django.db import models
from mobil.models import Car

# Create your models here.
class Transaksi(models.Model):
    namaPembeli = models.CharField(max_length=50)
    noHp = models.CharField(max_length=13)
    dari = models.DateTimeField(auto_now=False, auto_now_add=False)
    hingga = models.DateTimeField(auto_now=False, auto_now_add=False)
    mobil = models.ForeignKey(Car, on_delete=models.CASCADE, blank=True)