from django import forms
from .models import Transaksi

class TransaksiForm(forms.ModelForm):
    class Meta:
        model = Transaksi
        fields = ('namaPembeli', 'noHp', 'dari', 'hingga', 'mobil')