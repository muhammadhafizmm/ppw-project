from django.urls import path
from . import views

app_name = "transaksi"

urlpatterns = [
    path('sewa/<int:id>/', views.show, name='show'),
    path('terimakasih/', views.thanksGreeting, name='terimakasih'),
    path('riwayat/', views.riwayat, name='riwayat'),
    path('riwayat/detail/<int:id>/', views.riwayatDetail, name='riwayatDetail')
]