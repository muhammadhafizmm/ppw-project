from django.shortcuts import render, redirect
from .models import Transaksi
from .forms import TransaksiForm
from mobil.models import Car
from ulasan.models import Review
from datetime import datetime,date
import math

App_Name = "transaksi"

# Create your views here.
def show(request, id=0):
    mobil = Car.objects.get(id=id)
    if request.method == 'POST':
        url = request.POST
        mobil.sewa = False
        data = {
            'namaPembeli' : url['namaPembeli'],
            'noHp' : url['noHp'],
            'dari' : url['dari'],
            'hingga' : url['hingga'],
            'mobil' : mobil
            }
        form = TransaksiForm(data=data)
        if form.is_valid():
            dari = datetime.strptime(url["dari"] + " 00:00", '%m/%d/%Y %H:%M').timestamp()
            hingga = datetime.strptime(url["hingga"] + " 00:00", '%m/%d/%Y %H:%M').timestamp()
            todayDate = date.today()
            today = datetime.combine(todayDate, datetime.min.time()).timestamp()
            dateValidation = dari < today or hingga < today or hingga < dari
            if dateValidation:
                errorDate = "Waktu yang anda masukkan salah, coba masukkan lagi."
                return render(request, "transaksi/transaksi.html", {"errorDate" : errorDate, 'mobil' : mobil})
            else:
                form.save()
                mobil.save()
                return redirect('transaksi:terimakasih')
    return render(request, "transaksi/transaksi.html", {'mobil' : mobil})

def thanksGreeting(request):
    return render(request, "transaksi/terimakasih.html")

def riwayat(request):
    allTransaksi = Transaksi.objects.all()
    today = datetime.combine(date.today(), datetime.min.time()).timestamp()
    for item in allTransaksi:
        tanggalPengembalian = item.hingga.timestamp()
        if tanggalPengembalian < today:
            mobilId = item.mobil.id
            mobil = Car.objects.get(id=mobilId)
            mobil.sewa = True
            item.delete()
            mobil.save()
    allTransaksi = Transaksi.objects.all()
    return render(request, "transaksi/riwayat.html", {'transaksi' : allTransaksi})

def riwayatDetail(request, id=0):
    mobil = Car.objects.get(id=id)
    list_ulasan = Review.objects.filter(pk=id)
    rating = 5
    counter = 1
    for i in list_ulasan:
        rating += i.rating
        counter += 1
    rating = math.ceil(rating/counter)

    transaksi = Transaksi.objects.get(mobil=mobil)
    hingga = transaksi.hingga
        
    return render(request, "transaksi/detail.html", {'car' : mobil, 'rating': rating, 'hingga':hingga})
