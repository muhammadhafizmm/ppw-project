from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from mobil.models import Car
from .views import show, thanksGreeting, riwayat
from .models import Transaksi
from .apps import TransaksiConfig
from datetime import date, datetime
from .forms import TransaksiForm

# Create your tests here.
class TestTransaksiApp(TestCase):
	def test_apps(self):
		self.assertEqual(TransaksiConfig.name, 'transaksi')
		self.assertEqual(apps.get_app_config('transaksi').name, 'transaksi')

class TestRiwayat(TestCase):
	def test_riwayat_url_is_exist(self):
		response = Client().get('/transaksi/riwayat/')
		self.assertEqual(response.status_code, 200)

	def test_riwayat_index_func(self):
		found = resolve('/transaksi/riwayat/')
		self.assertEqual(found.func, riwayat)

	def test_riwayat_using_template(self):
		response = Client().get('/transaksi/riwayat/')
		self.assertTemplateUsed(response, 'transaksi/riwayat.html')

class TestTerimaKasih(TestCase):
	def test_terimakasih_url_is_exist(self):
		response = Client().get('/transaksi/terimakasih/')
		self.assertEqual(response.status_code, 200)

	def test_terimakasih_index_func(self):
		found = resolve('/transaksi/terimakasih/')
		self.assertEqual(found.func, thanksGreeting)

	def test_terimakasih_using_template(self):
		response = Client().get('/transaksi/terimakasih/')
		self.assertTemplateUsed(response, 'transaksi/terimakasih.html')

class TestDetailRiwayat(TestCase):
	def setUp(self):
		mobil = Car.objects.create(mobil="a", pemilik="b", tahun="1", kota="c", harga="d")
		transaksi = Transaksi.objects.create(
			namaPembeli='abc',
            noHp='123456789',
            mobil=mobil,
            dari=datetime.now(),
            hingga=datetime.now()
			)
		
	def test_detail_riwayat_url_is_exist(self):
		response = Client().get('/transaksi/riwayat/detail/1/')
		self.assertEqual(response.status_code, 200)
	
	def test_riwayat_using_template(self):
		mobil = Car.objects.create(mobil="a", pemilik="b", tahun="1", kota="c", harga="d")
		response = Client().get('/transaksi/riwayat/detail/1/')
		self.assertTemplateUsed(response, 'transaksi/detail.html')

class TestShow(TestCase):
	def test_show_using_template(self):
		mobil = Car.objects.create(mobil="a", pemilik="b", tahun="1", kota="c", harga="d")
		x = Car.objects.get(pk=1)
		response = Client().get('/transaksi/sewa/1/')
		self.assertTemplateUsed(response, 'transaksi/transaksi.html')

	def test_detail_riwayat_url_is_exist(self):
		mobil = Car.objects.create(mobil="a", pemilik="b", tahun="1", kota="c", harga="d")
		x = Car.objects.get(pk=1)
		time = date.today().strftime("%m/%d/%Y, %H:%M")
		context = {
            'namaPembeli' : 'a',
            'noHp' : '1',
            'dari' : time,
            'hingga' : time,
            'mobil' : mobil
            }
		form = TransaksiForm(data=context)
		response = Client().post('/transaksi/sewa/1/', context)
		self.assertEqual(response.status_code, 200)