from django.shortcuts import render, redirect
from .models import Review
from mobil.models import Car
from .forms import ReviewForm
from django.http import request
import math

# Create your views here.
def daftar_ulasan(request, index):
	car = Car.objects.get(pk=index)
	lst = Review.objects.filter(car=car)
	return render(request, 'ulasan/mobil_ulasan.html', {'lst': lst, 'car':car})

def tulis_ulasan(request, index):
	if request.method == 'POST':
		form = ReviewForm(request.POST)
		if form.is_valid():
			id_car = Review(
				car = Car.objects.get(id=index),
				review = form.data['review'],
				nama = form.data['nama'],
				rating = form.data['rating']
			)
			id_car.save()

			car = Car.objects.get(pk=index)
			ulasan = Review.objects.filter(car=car)
			rating = 0
			counter = 0
			for i in ulasan:
				rating += i.rating
				counter += 1

			car.rating = math.ceil(rating/counter)
			car.save()

			return redirect('transaksi:riwayatDetail', index)
	else:
		form = ReviewForm()

	return render(request, 'ulasan/add_review.html', {'form': form})