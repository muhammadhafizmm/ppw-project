from django.forms import ModelForm, forms
from .models import Review

class ReviewForm(ModelForm):
	class Meta:
		model = Review
		fields = [
			'nama',
			'rating',
			'review'
		]