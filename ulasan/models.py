from django.db import models
from mobil.models import Car

# Create your models here.
class Review(models.Model):
	rating_choices = [
		(1, 1),
		(2, 2),
		(3, 3),
		(4, 4),
		(5, 5),
	]
	car = models.ForeignKey(Car, on_delete=models.CASCADE, null=True)
	nama = models.CharField('Nama Penyewa', max_length=300, null=True)
	rating = models.IntegerField('Rating', choices=rating_choices, default=5)
	review = models.TextField('Isi ulasan', null=True)