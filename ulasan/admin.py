from django.contrib import admin
from .models import Review

# Register your models here.

@admin.register(Review)
class CarAdmin(admin.ModelAdmin):
    list_display = ('nama', 'rating', 'review')
    search_fields = ('nama',)