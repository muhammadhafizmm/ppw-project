from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps

from mobil.models import Car
from ulasan.models import Review

from .apps import UlasanConfig
from .forms import ReviewForm
from .views import tulis_ulasan, daftar_ulasan

# Create your tests here.
class TestUlasanApp(TestCase):
	def test_apps(self):
		self.assertEqual(UlasanConfig.name, 'ulasan')
		self.assertEqual(apps.get_app_config('ulasan').name, 'ulasan')

class TestReview(TestCase):
	def setUp(self):
		mobil = Car(mobil="a", pemilik="b", tahun="1", kota="c", harga="d")
		mobil.save()

	def test_review_index_func(self):
		found = resolve('/ulasan/tulis_ulasan/1/')
		self.assertEqual(found.func, tulis_ulasan)

	def test_review_using_template(self):
		response = Client().get('/ulasan/tulis_ulasan/1/')
		self.assertTemplateUsed(response, 'ulasan/add_review.html')

	def test_form_review_works(self):
		context = {
			'nama':'a',
			'review':'a',
			'rating':5,
		}
		response = Client().post('/ulasan/tulis_ulasan/1/', data=context)
		self.assertEqual(response.status_code, 302)

	def test_list_review(self):
		ulasan = Review(car=Car.objects.get(pk=1), nama='abc')
		ulasan.save()
		response = Client().get('/ulasan/1/')
		self.assertEqual(response.status_code, 200)