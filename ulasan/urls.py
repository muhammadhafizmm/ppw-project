from django.urls import path
from . import views

app_name = 'ulasan'

urlpatterns = [
    path('<int:index>/', views.daftar_ulasan, name='daftar_ulasan'),
    path('tulis_ulasan/<int:index>/', views.tulis_ulasan, name='tulis_ulasan')
]