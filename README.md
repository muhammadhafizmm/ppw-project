# Tugas Kelompok PPW

Anggota	:<br>
1806204966 - Jeremiah Garcia Tennes<br>
1906285610 - Fairuza Raryasdya Ayunda<br>
1906350742 - Muhammad Hafiz Maulana<br>
1906350761 - Farah Nazihah<br>
<br>
Status Pipelines :<br>
[![pipeline status](https://gitlab.com/muhammadhafizmm/ppw-project/badges/master/pipeline.svg)](https://gitlab.com/muhammadhafizmm/ppw-project/commits/master)
<br>
<br>
Status Code Coverage :<br>
[![coverage report](https://gitlab.com/muhammadhafizmm/ppw-project/badges/master/coverage.svg)](https://gitlab.com/muhammadhafizmm/ppw-project/commits/master)
<br>
<br>
Link :<br>
https://rentalalex-tkppw.herokuapp.com