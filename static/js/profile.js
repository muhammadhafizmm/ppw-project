$(document).ready(function() {
	$.ajaxSetup({ cache: false });

	console.log("START");
	$(document).on("click", ".click-heart", function(event) {
		var car_id = this.id;
		console.log("fav/unfav " + car_id);
		$.post('/mobil/add-fav/', {id:car_id}, function(data){
		});

		$('#'+car_id).remove();
	});

	$(document).on("click", ".card", function() {
		var car_id = this.id;
		console.log("click " + car_id);
		$.get('/mobil/detail/'+car_id, function(data){
		});
	});

	$(document).on("mouseenter", ".fa", function () {
		$(this).css("color", "black");
		$(this).attr('title', 'Hapus dari daftar favorit');
		$(this).tooltip();
	}).on("mouseleave", ".fa", function () {
		$(this).css("color", "red");
	});
});