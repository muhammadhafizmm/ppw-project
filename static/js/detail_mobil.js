$(document).ready(function() {
	$.ajaxSetup({ cache: false });

	console.log("START");
	$(document).on("click", ".click-heart", function () {
		var car_id = this.id;
		console.log("fav/unfav " + car_id);
		$.post('/mobil/add-fav/', {id:car_id}, function(data){
		});
		$(this).toggleClass("fa");
		$(this).toggleClass("far");
		$(this).toggleClass("red")
		$(this).toggleClass("black")
	});

	$(document).on("mouseenter", ".far", function () {
		$(this).css("color", "red");
		$(this).attr('title', 'Tambahkan ke daftar favorit');
		$(this).tooltip();
	}).on("mouseleave", ".far", function () {
		$(this).css("color", "black");
	});

	$(document).on("mouseenter", ".fa", function () {
		$(this).css("color", "black");
		$(this).attr('title', 'Hapus dari daftar favorit');
		$(this).tooltip();
	}).on("mouseleave", ".fa", function () {
		$(this).css("color", "red");
	});
});