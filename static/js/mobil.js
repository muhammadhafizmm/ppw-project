
$(document).ready(function() {
    let listMobil = []
    $.ajax({
        url: window.location.origin + '/mobil/getAllCarData/',
        type: 'get',
        dataType: 'json',
        success: function(result) {
            let deskripsiSnippet = ''
            if(result.length > 0){
                $.each(result, function(i, data){
                    listMobil.push(data)
                    deskripsiSnippet = data.fields.deskripsi.slice(0, 50) + '...'
                    if (data.fields.sewa) {
                        $("#carList").append(`
                            <div class="card m-3 p-3 shadow" style="width: 450px; cursor:pointer;" onclick="location.href='${window.location.href + "detail/" + data.pk + "/"}'">
                                <img src="/static/img/mobil/car-black_3.png" class="card-img-top" alt="">
                                <div class="card-body">
                                    <h5 class="card-title">${data.fields.mobil}</h5>
                                    <h6 class="card-subtitle mb-2 text-muted">${data.fields.harga}</h6>
                                    <p class="card-text">${deskripsiSnippet}</p>
                                </div>
                            </div>
                        `)
                    }
                })
            } else {
                $("#carList").append(`
                    <div class="content-empty">
                        <h1 class="message">Yaah.. Content Masih Kosong</h1>
                    </div>
                `)
            }
            // take random mobil for suggestion
            let mobilRandom = []
            for (let index = 0; index < 10; index++) {
                if (listMobil.length > 10) {
                    mobilRandom.push(listMobil[Math.floor(Math.random() * listMobil.length)])
                } else {
                    if (listMobil[index] != null) {
                        mobilRandom.push(listMobil[index])
                    }
                }
            }
            $.each(mobilRandom, function(i, data){
                $("#suggestionBox").append(`<span class="name-suggest">${data.fields.mobil}</span>`)
            })
        }
    })

    // click to open search mode
    $("#search-bar").on('click', function(){
        $("#suggestionBox").css("display", "flex")
        $(".overlay-background").css("display", "block")
    })

    // click to close search mode
    $(".overlay-background").on('click', function(){
        $("#suggestionBox").css("display", "none")
        $(".overlay-background").css("display", "none")
    })

    // search car
    $("#search-bar").on('keyup', function(event){
        $("#suggestionBox").css("display", "flex")
        $(".overlay-background").css("display", "block")
        if(event.which != 13){
            searchSuggest()
        } else {
            searchSubmit($("#search-bar").val())
        }
    })

    $("#suggestionBox").on('click', '.name-suggest', function(){
        searchSubmit($(this).text())
    })

    // Karena di pake lebih dari sekali, biar enak di masukin function aja
    function searchSuggest(){
        // kasus kasus
        let mobilSuggestTampil
        if (isNaN($("#search-bar").val())) { // kalau dia string
            mobilSuggestTampil = listMobil.filter(function(data){
                const mobil = data.fields.mobil
                if (mobil.toLowerCase().includes($("#search-bar").val().toLowerCase())){
                    return data
                }
            })
        } else if($("#search-bar").val() == ""){ // kalau dia kosong
            // random car suggest
            mobilSuggestTampil = []
            for (let index = 0; index < 10; index++) {
                if (listMobil.length > 10) {
                    mobilSuggestTampil.push(listMobil[Math.floor(Math.random() * listMobil.length)])
                } else {
                    if (listMobil[index] != null) {
                        mobilSuggestTampil.push(listMobil[index])
                    }
                }
            }
        } else if (!isNaN($("#search-bar").val())) { // kalau  dia angka
            mobilSuggestTampil = listMobil.filter(function(data){
                const harga = data.fields.harga.replace(/[Rp.]/g, "")
                if (Number(harga) <= Number($("#search-bar").val())) {
                    return data
                }
            })
        }

        // render component
        $("#suggestionBox").html('')
        if (mobilSuggestTampil.length > 0) {
            for (let index = 0; index < 10; index++) {
                if (mobilSuggestTampil[index] != null) {
                    $("#suggestionBox").append(`<span class="name-suggest">${mobilSuggestTampil[index].fields.mobil}</span>`)
                }
            }
        }
    }

    // Karena di pake lebih dari sekali, biar enak di masukin function aja
    function searchSubmit(param) {
        let mobilTampil
        if (param == ""){ // kalau string
            mobilTampil = [...listMobil]
        } else if (!isNaN(param)){ // kalau number
            mobilTampil = listMobil.filter(function(data){
                let harga = data.fields.harga.replace(/[Rp.]/g, "")
                if (Number(harga) <= Number(param)){
                    return data
                }
            }) 
        } else if (isNaN(param)){
            mobilTampil = listMobil.filter(function(data){
                let namaMobil = data.fields.mobil
                if (namaMobil.toLowerCase().includes(param.toLowerCase())){
                    return data
                }
            })
        }
        $("#carList").html('')
        $.each(mobilTampil, function(i, data){
            deskripsiSnippet = data.fields.deskripsi.slice(0, 50) + '...'
            if (data.fields.sewa) {
                $("#carList").append(`
                    <div class="card m-3 p-3 shadow" style="width: 450px; cursor:pointer;" onclick="location.href='${window.location.href + "detail/" + data.pk + "/"}'">
                        <img src="/static/img/mobil/car-black_3.png" class="card-img-top" alt="">
                        <div class="card-body">
                            <h5 class="card-title">${data.fields.mobil}</h5>
                            <h6 class="card-subtitle mb-2 text-muted">${data.fields.harga}</h6>
                            <p class="card-text">${deskripsiSnippet}</p>
                        </div>
                    </div>
                `)
            }
        })
        $("#suggestionBox").css("display", "none")
        $(".overlay-background").css("display", "none")
    }
})