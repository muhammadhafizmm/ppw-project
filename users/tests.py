from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
import time

from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver

from django.apps import apps
from .apps import UsersConfig
from .views import login_page, index, signup_page, profile_page, logout_acc, edit_profile

# Create your tests here.
class UsersUnitTest(TestCase):
    def test_apps(self):
        self.assertEqual(UsersConfig.name, 'users')
        self.assertEqual(apps.get_app_config('users').name, 'users')

    def test_home_url_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_profile_url_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_login_url_exist(self):
        response = Client().get('/login/')
        self.assertEqual(response.status_code, 200)

    def test_logout_url_exist(self):
        response = Client().get('/logout/')
        self.assertEqual(response.status_code, 302)

    def test_signup_url_exist(self):
        response = Client().get('/signup/')
        self.assertEqual(response.status_code, 200)

    def test_update_profile_url_exist(self):
        response = Client().get('/edit_profile/')
        self.assertEqual(response.status_code, 200)

    def test_home_function(self):
        self.found = resolve('/')
        self.assertEqual(self.found.func, index)

    def test_profile_function(self):
        self.found = resolve('/profile/')
        self.assertEqual(self.found.func, profile_page)

    def test_signup_function(self):
        self.found = resolve('/signup/')
        self.assertEqual(self.found.func, signup_page)

    def test_login_function(self):
        self.found = resolve('/login/')
        self.assertEqual(self.found.func, login_page)

    def test_logout_function(self):
        self.found = resolve('/logout/')
        self.assertEqual(self.found.func, logout_acc)

    def test_update_data_function(self):
        self.found = resolve('/edit_profile/')
        self.assertEqual(self.found.func, edit_profile)

class FuncTestAuth(LiveServerTestCase):
    def setUp(self):
        opt = Options()
        opt.add_argument('--no-sandbox') 
        opt.add_argument('--headless')
        opt.add_argument('disable-gpu')
        opt.add_argument('--disable-dev-shm-usage')
        self.browser = webdriver.Chrome('./chromedriver', chrome_options=opt)
        super(FuncTestAuth, self).setUp()

    def tearDown(self):
        self.browser.quit()
        super(FuncTestAuth, self).tearDown()
    
    def test_signup(self):
        self.browser.get(self.live_server_url + "/")
        self.browser.implicitly_wait(10)

        # menuju signup page
        signup_page = self.browser.find_element_by_id('login')
        signup_page.click()
        time.sleep(2)

        signup_page = self.browser.find_element_by_id('signup')
        signup_page.click()
        time.sleep(2)

        # mengisi data untuk sign up
        nama = self.browser.find_element_by_id('nama')
        nama.send_keys('Alex')
        notlp = self.browser.find_element_by_id('phone')
        notlp.send_keys('088888888888')
        email = self.browser.find_element_by_id('email')
        email.send_keys('alex@gmail.com')
        tgl = self.browser.find_element_by_id('tgl_lahir')
        tgl.send_keys('10022000')
        kota = self.browser.find_element_by_id('kota')
        kota.send_keys('Depok')
        uname = self.browser.find_element_by_id('username')
        uname.send_keys('canismajoris')
        passw = self.browser.find_element_by_id('password')
        passw.send_keys('sirius')

        signup_submit = self.browser.find_element_by_id('signup-submit')
        signup_submit.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()
        time.sleep(2)

        # mengecek username ada di layar
        page_text_signup = self.browser.find_element_by_tag_name('body').text
        self.assertIn('canismajoris', page_text_signup)

        # log out
        logout_btn = self.browser.find_element_by_id('logout-btn')
        logout_btn.click()

        alert = self.browser.switch_to.alert
        alert.accept()
        time.sleep(2)

        # menuju login page
        signup_page = self.browser.find_element_by_id('login')
        signup_page.click()
        time.sleep(2)

        # mengisi data untuk login
        uname = self.browser.find_element_by_id('username')
        uname.send_keys('canismajoris')
        passw = self.browser.find_element_by_id('password')
        passw.send_keys('sirius')

        signup_submit = self.browser.find_element_by_id('login-submit')
        signup_submit.click()
        time.sleep(2)

        nav_akun = self.browser.find_element_by_id('drop-akun')
        nav_akun.click()
    
        nav_logout = self.browser.find_element_by_id('logout-nav')
        nav_logout.click()

    def test_ubah_data(self):
        self.browser.get(self.live_server_url + "/signup/")
        self.browser.implicitly_wait(10)

        # mengisi data untuk sign up
        nama = self.browser.find_element_by_id('nama')
        nama.send_keys('Alex')
        notlp = self.browser.find_element_by_id('phone')
        notlp.send_keys('088888888888')
        email = self.browser.find_element_by_id('email')
        email.send_keys('alex@gmail.com')
        tgl = self.browser.find_element_by_id('tgl_lahir')
        tgl.send_keys('10022000')
        kota = self.browser.find_element_by_id('kota')
        kota.send_keys('Depok')
        uname = self.browser.find_element_by_id('username')
        uname.send_keys('canismajoris')
        passw = self.browser.find_element_by_id('password')
        passw.send_keys('sirius')

        signup_submit = self.browser.find_element_by_id('signup-submit')
        signup_submit.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()
        time.sleep(2)

        email_before = self.browser.find_element_by_id('email-data').text

        ubah_btn = self.browser.find_element_by_id('ubah')
        ubah_btn.click()
        time.sleep(2)

        notlp = self.browser.find_element_by_id('phone')
        notlp.send_keys('088888888888')
        email = self.browser.find_element_by_id('email')
        email.send_keys('alex_ppw@gmail.com')
        kota = self.browser.find_element_by_id('kota')
        kota.send_keys('Depok')
        uname = self.browser.find_element_by_id('username')
        uname.send_keys('canismajoris')
        passw = self.browser.find_element_by_id('password')
        passw.send_keys('sirius')

        update_btn = self.browser.find_element_by_id('update-submit')
        update_btn.click()
        time.sleep(2)

        email_after = self.browser.find_element_by_id('email-data').text

        self.assertNotEqual(email_before, email_after)

    def test_login_without_make_account(self):
        self.browser.get(self.live_server_url + "/login/")
        self.browser.implicitly_wait(10)

        # mengisi data untuk login
        uname = self.browser.find_element_by_id('username')
        uname.send_keys('huahaha')
        passw = self.browser.find_element_by_id('password')
        passw.send_keys('huehehe')

        signup_submit = self.browser.find_element_by_id('login-submit')
        signup_submit.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()

    def test_empty_login(self):
        self.browser.get(self.live_server_url + "/login/")
        self.browser.implicitly_wait(10)

        login_submit = self.browser.find_element_by_id('login-submit')
        login_submit.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()

    def test_empty_signin(self):
        self.browser.get(self.live_server_url + "/signup/")
        self.browser.implicitly_wait(10)

        login_submit = self.browser.find_element_by_id('signup-submit')
        login_submit.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()

    def test_exist_account(self):
        self.browser.get(self.live_server_url + "/signup/")
        self.browser.implicitly_wait(10)

        # mengisi data untuk sign up
        nama = self.browser.find_element_by_id('nama')
        nama.send_keys('Alex')
        notlp = self.browser.find_element_by_id('phone')
        notlp.send_keys('088888888888')
        email = self.browser.find_element_by_id('email')
        email.send_keys('alex@gmail.com')
        tgl = self.browser.find_element_by_id('tgl_lahir')
        tgl.send_keys('10022000')
        kota = self.browser.find_element_by_id('kota')
        kota.send_keys('Depok')
        uname = self.browser.find_element_by_id('username')
        uname.send_keys('canismajoris')
        passw = self.browser.find_element_by_id('password')
        passw.send_keys('sirius')

        signup_submit = self.browser.find_element_by_id('signup-submit')
        signup_submit.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()
        time.sleep(2)

        self.browser.get(self.live_server_url + "/signup/")
        self.browser.implicitly_wait(10)

        # mengisi data untuk sign up
        nama = self.browser.find_element_by_id('nama')
        nama.send_keys('Alex')
        notlp = self.browser.find_element_by_id('phone')
        notlp.send_keys('088888888888')
        email = self.browser.find_element_by_id('email')
        email.send_keys('alex@gmail.com')
        tgl = self.browser.find_element_by_id('tgl_lahir')
        tgl.send_keys('10022000')
        kota = self.browser.find_element_by_id('kota')
        kota.send_keys('Depok')
        uname = self.browser.find_element_by_id('username')
        uname.send_keys('canismajoris')
        passw = self.browser.find_element_by_id('password')
        passw.send_keys('sirius')

        signup_submit = self.browser.find_element_by_id('signup-submit')
        signup_submit.click()
        time.sleep(2)

        alert = self.browser.switch_to.alert
        alert.accept()

    # def test_ubah_data_not_complete(self):
    #     self.browser.get(self.live_server_url + "/signup/")
    #     self.browser.implicitly_wait(10)

    #     # mengisi data untuk sign up
    #     nama = self.browser.find_element_by_id('nama')
    #     nama.send_keys('Alex')
    #     notlp = self.browser.find_element_by_id('phone')
    #     notlp.send_keys('088888888888')
    #     email = self.browser.find_element_by_id('email')
    #     email.send_keys('alex@gmail.com')
    #     tgl = self.browser.find_element_by_id('tgl_lahir')
    #     tgl.send_keys('10022000')
    #     kota = self.browser.find_element_by_id('kota')
    #     kota.send_keys('Depok')
    #     uname = self.browser.find_element_by_id('username')
    #     uname.send_keys('canismajoris')
    #     passw = self.browser.find_element_by_id('password')
    #     passw.send_keys('sirius')

    #     signup_submit = self.browser.find_element_by_id('signup-submit')
    #     signup_submit.click()
    #     time.sleep(2)

    #     alert = self.browser.switch_to.alert
    #     alert.accept()
    #     time.sleep(2)

    #     ubah_btn = self.browser.find_element_by_id('ubah')
    #     ubah_btn.click()
    #     time.sleep(2)

    #     notlp = self.browser.find_element_by_id('phone')
    #     notlp.send_keys('088888888888')
    #     kota = self.browser.find_element_by_id('kota')
    #     kota.send_keys('Depok')
    #     uname = self.browser.find_element_by_id('username')
    #     uname.send_keys('canismajoris')
    #     passw = self.browser.find_element_by_id('password')
    #     passw.send_keys('sirius')

    #     update_btn = self.browser.find_element_by_id('update-submit')
    #     update_btn.click()
    #     time.sleep(2)

    #     alert = self.browser.switch_to.alert
    #     alert.accept()