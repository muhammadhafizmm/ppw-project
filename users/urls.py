from django.urls import path
from . import views

app_name = "users"

urlpatterns = [
    path('', views.index, name='index'),
    path('profile/', views.profile_page, name='profile'),
    path('signup/', views.signup_page, name='signup'),
    path('login/', views.login_page, name='login'),
    path('logout/', views.logout_acc, name='logout'),
    path('edit_profile/', views.edit_profile, name='edit_profile')
]