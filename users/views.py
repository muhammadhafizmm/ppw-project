from django.shortcuts import redirect, render
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib import messages
from .models import Profile

# Create your views here.
def index(request):
    home = True
    return render(request, "home.html", {'home' : home})

def profile_page(request):
    if request.user.is_authenticated:
        fav_car = request.user.profile.favorit.all()
        return render(request, "users/profile.html", {'fav_car': fav_car} )
    else:
        return render(request, "users/profile.html")

def signup_page(request):
    if request.method == 'POST':
        nama = request.POST['nama']
        no_hp = request.POST['phone']
        email = request.POST['email']
        tgl_lahir = request.POST['tgl_lahir']
        kota = request.POST['kota']
        username = request.POST['username']
        password = request.POST['password']

        if(not username or not password):
            messages.error(request, "Complete your data first")
            return render(request, 'users/signup.html')

        if User.objects.filter(username__iexact=username).exists():
            messages.error(request, "Username already exist.")
            return render(request, 'users/signup.html')
        else:
            user = User.objects.create_user(username=username, password=password, email=email)
            user.profile.tgl_lahir = tgl_lahir
            user.profile.nama = nama
            user.profile.no_hp = no_hp
            user.profile.kota = kota
            user.save()

            messages.success(request, "Successfully signed in.")

            user_auth = authenticate(request, username=username, password=password)
            login(request, user_auth)

            return redirect('/profile/')
    else:
        return render(request, 'users/signup.html')

def login_page(request):
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']

        if(not username or not password):
            messages.error(request, "Complete your data first")
            return render(request, 'users/login.html')

        user = authenticate(request, username=username, password=password)
        
        if user is not None:
            login(request, user)
            return redirect('/')
        else:
            messages.error(request, "Username or password is incorrect.")
            return render(request, 'users/login.html')
    
    else:      
        return render(request, 'users/login.html')

def logout_acc(request):
    logout(request)
    messages.success(request, "Successfully logged out.")
    return redirect('/')

def edit_profile(request):
    if request.method == 'POST':
        uname = request.POST['username']
        password = request.POST['password']
        no_hp = request.POST['phone']
        kota = request.POST['kota']
        email = request.POST['email']

        if (no_hp is not None) and (kota is not None) and (email is not None):
            user = authenticate(request, username=uname, password=password)
            user.profile.no_hp = no_hp
            user.profile.kota = kota
            user.email = email
            user.save()
        else:
            messages.error(request, "Please complete all your data first.")
            return redirect('/profile')
    
        return redirect('/profile')
    else:
        return render(request, 'users/ubah_data.html')