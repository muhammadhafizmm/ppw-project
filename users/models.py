from django.db import models
from django.dispatch import receiver
from django.db.models.signals import post_save
from django.contrib.auth.models import User
from mobil.models import Car

# Create your models here.
class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    tgl_lahir = models.DateField('tgl lahir', null=True, blank=True)
    nama = models.CharField('nama', max_length=100, null=True)
    no_hp = models.CharField('hp', max_length=20, null=True)
    kota = models.CharField('kota', blank=True, max_length=200, null=True)
    favorit = models.ManyToManyField(Car)

@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()